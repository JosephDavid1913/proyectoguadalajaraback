'use strict';

module.exports = function(Courses) {
    Courses.observe('access', function logQuery(ctx, next) {
        console.log('ctx',ctx);
        console.log('Accessing %s matching %s', ctx.Model.modelName, ctx.query.where);
        next();
    });
};
