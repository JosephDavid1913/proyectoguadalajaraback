'use strict';

module.exports = function(Container) {

    Container.disableRemoteMethod('destroyContainer', true);
    Container.disableRemoteMethod('removeFile', true);
    Container.beforeRemote('upload', function(ctx, res, next) {
        console.log("subiendo imagen");
        next();
    });
};
